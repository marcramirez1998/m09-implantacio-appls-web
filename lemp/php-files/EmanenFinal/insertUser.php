<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercici 3</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #f2f2f2;
        }
        body {
            background-color: #f2f2f2;
            text-align: center;
        }

    </style>
</head>
<body>
    <h2>Inserir users</h2>
    <form action="" method="POST">
        <label for="first_name">Nom:</label>
        <input type="text" id="first_name" name="first_name" required>
        <label for="last_name">Cognom:</label>
        <input type="text" id="last_name" name="last_name" required><br><br>
        <label for="email">Gmail:</label>
        <input type="text" id="email" name="email" required>
        <label for="password">Password:</label>
        <input type="text" id="password" name="password" required><br><br>
        <label for="status_id">Status ID:</label>
        <input type="text" id="status_id" name="status_id" required>
        <label for="info">User Info:</label>
        <input type="text" id="info" name="info" required><br><br>
        <input type="submit" value="Insert">
    </form>
    <br>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $status_id = $_POST['status_id'];
        $info = $_POST['info'];
        $servername = "192.168.80.2";
        $username = "root";
        $password = "1234";
        $dbname = "bookstore";
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        class User
        {
            private $first_name;
            private $last_name;
            private $email;
            private $password;
            private $status_id;
            private $info;
            public function __construct($first_name, $last_name, $email, $password, $status_id, $info)
            {
                $this->first_name = $first_name;
                $this->last_name = $last_name;
                $this->email = $email;
                $this->password = $password;
                $this->status_id = $status_id;
                $this->info = $info;
            }
            public function insertUser($conn)
            {
                $sql = "INSERT INTO users (user_first_name, user_last_name, user_email, user_password, user_status_id, user_info) 
                        VALUES ('$this->first_name', '$this->last_name', '$this->email', '$this->password', $this->status_id, '$this->info')";
                
                if ($conn->query($sql) === TRUE) {
                    echo "Inserit";
                } else {
                    echo "Error no Inserit:" . $conn->error;
                }
            }
        }
        $user = new User($first_name, $last_name, $email, $password, $status_id, $info);
        $user->insertUser($conn);
        $conn->close();
    }
    ?>
</body>
</html>