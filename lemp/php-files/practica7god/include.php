<?php
function factorial($n) {
    if($n <= 1) {
        return 1;
    } else {
        return $n * factorial($n - 1);
    }
}

$factorialResultat = ""; 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $numero = $_POST['number'];
    
    $factorial = factorial($numero);
    
    $factorialResultat = "$numero! = $factorial";
}

$factorialTabla = "<h2>Tabla de factoriales del 0 al 100:</h2>";
$factorialTabla .= "<table border='1'>";
$factorialTabla .= "<tr><th>n</th><th>n!</th></tr>";
for ($i = 0; $i <= 100; $i++) {
    $factorialTabla .= "<tr><td>$i</td><td>" . factorial($i) . "</td></tr>";
}
$factorialTabla .= "</table>";
?>
