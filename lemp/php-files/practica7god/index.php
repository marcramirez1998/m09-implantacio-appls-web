<!DOCTYPE HTML>
<html>
<head>
    <title>Actividad 5 PHP: FACTORIAL</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
    <h1>Calculadora Factorial</h1>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <label>Introduce un número para calcular su factorial:</label><br>
                <label for="number">Número:</label>
                <input type="number" id="number" name="number" min="0" max="100" required>
                <br><br>
                <button type="submit">Calcular</button>
                <?php include 'include.php'; ?> 
                <?php echo $factorialResultat; ?>
            </form>
    <h1>Factorial Formulas</h1>
    
    <h2>The formula to calculate a factorial for a number is:</h2>
    <p>n! = n × (n-1) × (n-2) × … × 1</p>
    <p>Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the number 1 is reached.</p>
    <p>The factorial of zero is 1:</p>
    <p>0! = 1</p>
    
    <h2>Recurrence Relation</h2>
    <p>And the formula expressed using the recurrence relation looks like this:</p>
    <p>n! = n × (n – 1)!</p>
    <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recurses until n is equal to 0.</p>
    
    <h2>Factorial Table</h2>
    <p>The table below shows the factorial n! for the numbers one to one-hundred.</p>
    
    <?php echo $factorialTabla; ?>

</body>
</html>
