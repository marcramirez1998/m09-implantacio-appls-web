<?php
// Dades de les vendes en milions
$vendes = array(
    "Playstation 2" => 155,
    "Nintendo DS" => 154,
    "Game Boy" => 119,
    "Play Station 4" => 102,
    "Wii" => 101,
    "Play Station 3" => 87,
    "Xbox 360" => 84,
    "Play Station Portable" => 82,
    "Game Boy Advance" => 81,
    "Nintendo 3DS" => 72,
    "Nes" => 62,
    "Nintendo Switch" => 60
);

$max_vendes = max($vendes);

echo "<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    th, td {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    tr:nth-child(even) {
        background-color: #f2f2f2;
    }
    .graph {
        display: flex;
        justify-content: center;
    }
    .graph div {
        max-width: 150px; /* Ajustar el ancho máximo de la barra */
    }
</style>";

echo "<table>";
echo "<tr><th>Consola</th><th>Ventes</th><th>Total Vendes</th></tr>";
foreach ($vendes as $consola => $total_vendes) {
    $amplada_barra_verda = ($total_vendes / $max_vendes) * 500; 
    echo "<tr>";
    echo "<td>$consola</td>";
    echo "<td class='graph'><div style='background-color: green; width: {$amplada_barra_verda}px; height: 20px;'></div></td>";
    echo "<td>$total_vendes</td>";
    echo "</tr>";
}
echo "</table>";
?>
