<?php
$priceErr = "";
$taxErr = "";
$price = $tax = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["price"])) {
        $priceErr = "El preu es un requisit";
    } else {
        $price = validate($_POST["price"]);
        if (!is_numeric($price)) {
            $priceErr = "El preu ha de ser un valor numèric";
        }
    }

    if (empty($_POST["tax"])) {
        $taxErr = "La taxa es un requisit";
    } else {
        $tax = validate($_POST["tax"]);
        if (!is_numeric($tax)) {
            $taxErr = "La taxa ha de ser un valor numèric";
        }
    }
}


function validate($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>
