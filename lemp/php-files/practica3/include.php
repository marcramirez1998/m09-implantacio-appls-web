<?php
        for ($grados = 0; $grados <= 360; $grados++) {
            $radianes = deg2rad($grados);
            $seno = sin($radianes);
            $coseno = cos($radianes);
            echo "<tr>";
            echo "<td><FONT COLOR='blue'>$grados</td>";
            echo "<td><FONT COLOR='blue'>" . number_format($radianes, 4) . "</td>";
            echo "<td style='color:" . ($seno < 0 ? 'red' : 'blue') . ";'>" . number_format($seno, 4) . "</td>";
            echo "<td style='color:" . ($coseno < 0 ? 'red' : 'blue') . ";'>" . number_format($coseno, 4) . "</td>";
            echo "</tr>";
        }
?>