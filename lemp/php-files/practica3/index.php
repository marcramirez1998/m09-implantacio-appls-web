<!DOCTYPE html>
<html>
<head>
    <title>Tabla de Seno y Coseno</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1 style="text-align: center;">Sine and Cosine</h1>
    <img src="imagen.jpg" width="30%" height="30%" style="display: block; margin-left: auto; margin-right: auto;">
    </br>
    <table border="1">
        <tr>
            <th>Degrees</th>
            <th>Radians</th>
            <th>Sine</th>
            <th>Cosine</th>
        </tr>
        <?php include 'include.php'; ?>
    </table>
</body>
</html>
