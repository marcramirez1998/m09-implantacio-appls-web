<!DOCTYPE HTML>
<html>
<head>
    <title>Activitat 5 PHP: TAX</title>
    <style>
        .error {color: #FF0000;}
    </style>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<h1>PRICE, TAX and ROUNDS</h1>
<?php
    $priceErr = "";
    $taxErr = "";
    $price = $tax = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["price"])) {
            $priceErr = "Price is required";
        } else {
            $price = validate($_POST["price"]);
        }

        if (empty($_POST["tax"])) {
            $taxErr = "Tax is required";
        } else {
            $tax = validate($_POST["tax"]);
        }
    }

    function validate($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>
    
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    
    <label for="price">Price with TAX:</label>
    <br />
    <input type="text" name="price" value="<?php echo $price?>"/><span class="error">*<?php echo $priceErr;?></span>
    <br />
    <label for="tax">Tax(%):</label>
    <br />
    <input type="text" name="tax" value="<?php echo $tax?>"/><span class="error">*<?php echo $taxErr;?></span>
    <br />
    <br />
    <input type="submit" value="Calculate" />
    <br />
    <h1>TAX data</h1>
</form>

<?php 
    if(isset($_POST["price"]) && isset($_POST["tax"]) && !empty($_POST["price"]) && !empty($_POST["tax"])) {
        $price_with_tax = floatval($_POST["price"]);
        $tax_percentage = floatval($_POST["tax"]);
        $price_without_tax = $price_with_tax / (1 + $tax_percentage / 100);
        
        echo 'Price without tax: ' . $price_without_tax . '<br />';
        
        echo 'Round to 2 decimals using round(): ' . round($price_without_tax, 2) . '<br />';
        
        echo 'Using function sprintf: ' . sprintf("%.4f", $price_without_tax ) . '<br />';
    }
?>

</body>
</html>
