<?php
$vendes = array(
    "Nintendo Switch" => 60,
    "Game Boy Advance" => 81,
    "Play Station 4" => 102,
    "Xbox 360" => 84,
    "Nes" => 62,
    "Playstation 2" => 1555,
    "Wii" => 101,
    "Play Station 3" => 87,
    "Play Station Portable" => 82,
    "Nintendo 3DS" => 72,
    "Nintendo DS" => 154,
    "Game Boy" => 119
);

$max_vendes = max($vendes);

echo "<table class='table table-dark table-striped'>";
echo "<tr>";
echo "<th>Console</th>";
echo "<th>Sales</th>";
echo "</tr>";

arsort($vendes);
foreach ($vendes as $consola => $total_vendes) {
    $imatge = ($total_vendes / $max_vendes) * 500; 
    echo "<tr>";
    echo "<td>$consola</td>";
    echo "<td>";
    echo "<div>";
    echo "<div style='background-image: url(green.png); background-repeat: repeat-x; background-size: contain; width: {$imatge}px; height: 20px;'></div>";
    echo "<span style='margin-left: 5px;'>$total_vendes</span>";
    echo "</div>";
    echo "</td>";
    echo "</tr>";
}

echo "</table>";
?>
