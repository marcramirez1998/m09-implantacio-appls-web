<!DOCTYPE html>
<html>
<head>
    <title>Tabla de Seno y Coseno</title>
    <meta charset="UTF-8">
    <title>Video Games Sales</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1 class="center">Tabla de Seno y Coseno</h1>
    <img src="imagen.jpg" class="center">

    <table border="1">
        <tr>
            <th>Ángulo (grados)</th>
            <th>Ángulo (radianes)</th>
            <th>Seno</th>
            <th>Coseno</th>
        </tr>
        <?php
        for ($grados = 0; $grados <= 360; $grados++) {
            $radianes = deg2rad($grados);
            $seno = sin($radianes);
            $coseno = cos($radianes);
            echo "<tr>";
            echo "<td>$grados</td>";
            echo "<td>" . number_format($radianes, 4) . "</td>";
            echo "<td style='color:" . ($seno < 0 ? 'red' : 'black') . ";'>" . number_format($seno, 4) . "</td>";
            echo "<td style='color:" . ($coseno < 0 ? 'red' : 'black') . ";'>" . number_format($coseno, 4) . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>
</html>

