<?php
    $array1 = array("RED", "BLUE", "GREEN");
    $array2 = array("CYAN","YELLOW", "MAGENTA", "BLACK");

    echo $array1[0] . "<br/>";
    echo $array1[1] . "<br/>";
    echo $array1[2] . "<br/>";

    $len = count($array1);

    for ($i = 0; $i < $len; $i++) {
        echo $array1[$i] . "<br/>";
    }

    $games = array(
        "playstation 2" => "RED",
        "GAMEBOY" => "GREEN",
        "WII" => "BLUE",
    );
    foreach ($games as $key => $value) {
        echo $key . ":" . $value . "<br/>";
    }
    //echo $games["GAMEBOY"] . "<br/>";

    $cars = array (
        array("Volvo",22,18),
        array("BMW",15,13),
        array("Saab",5,2),
        array("Land Rover",17,15)
      );
      echo $cars[0][0] . ": In stock: " . $cars[0][1] . ", sold: " . $cars[0][2] . ".<br>";
      echo $cars[1][0] . ": In stock: " . $cars[1][1] . ", sold: " . $cars[1][2] . ".<br>";
      echo $cars[2][0] . ": In stock: " . $cars[2][1] . ", sold: " . $cars[2][2] . ".<br>";
      echo $cars[3][0] . ": In stock: " . $cars[3][1] . ", sold: " . $cars[3][2] . ".<br>";

    
      for ($row = 0; $row < 4; $row++) {
        echo "<p><b>Row number $row</b></p>";
        echo "<ul>";
        for ($col = 0; $col < 3; $col++) {
            echo "<li>" . $cars[$row][$col] . "</li>";
        }
        echo "</ul>";
    }
?>
