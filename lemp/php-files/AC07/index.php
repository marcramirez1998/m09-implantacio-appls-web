<!DOCTYPE HTML>
<html>
<head>
    <title>Actividad 5 PHP: FACTORIAL</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async
    src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
</head>
<body>
<h1>Calculadora de Potencia</h1>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <label for="base">Base:</label><br>
        <input type="number" id="base" name="base" required value="<?php echo $base?>"/><br> 
        <label for="exponent">Exponent:</label><br>
        <input type="number" id="exponent" name="exponent" required value="<?php echo $exponent?>"/><br> 
        <button type="submit">Calcular</button><br>
        <?php include 'include.php'; ?> 
    </form>
    <h2>Positive exponent</h2>
    <p>Exponentiation is a mathematical operation, written as x<sup>n</sup>, involving the base x and an exponent n. In the case where n is a positive integer, exponentiation corresponds to repeated multiplication of the base, n times.</p>
    <p>The calculator above accepts negative bases, but does not compute imaginary numbers. It also does not accept fractions, but can be used to compute fractional exponents, as long as the exponents are input in their decimal form.</p>

    <h2>Negative exponent</h2>
    <p>When an exponent is negative, the negative sign is removed by reciprocating the base and raising it to the positive exponent.</p>
    <p>So the factorial n! is equal to the number n times the factorial of n minus one. This recurses until n is equal to 0.</p>

    <h2>Exponent 0</h2>
    <p>When an exponent is 0, any number raised to the 0 power is 1.</p>
    <p>For 0 raised to the 0 power the answer is 1 however this is considered a definition and not an actual calculation.</p>

    <h2>Real number exponent</h2>
    <p>For real numbers, we use the PHP function <code>pow(n,x)</code>.</p>
</body>
</html>
