<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $base = $_POST['base'];
        $exponent = $_POST['exponent'];
        
        $result = 1;
        if ($exponent >= 0 && is_int($exponent)) {
            for ($i = 1; $i <= $exponent; $i++) {
                $result *= $base;
            }
        } elseif ($exponent < 0 && is_int($exponent)) {
            for ($i = 1; $i <= abs($exponent); $i++) {
                $result *= (1 / $base);
            }
        } else {
            $result = pow($base, $exponent);
        }

        echo "<p>x<sup>n=</sup>: $result</p>";
    }
    ?>