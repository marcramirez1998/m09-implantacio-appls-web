<?php
$vendes = array(
    "Playstation 2" => 155,
    "Nintendo DS" => 154,
    "Game Boy" => 119,
    "Play Station 4" => 102,
    "Wii" => 101,
    "Play Station 3" => 87,
    "Xbox 360" => 84,
    "Play Station Portable" => 82,
    "Game Boy Advance" => 81,
    "Nintendo 3DS" => 72,
    "Nes" => 62,
    "Nintendo Switch" => 60
);

$max_vendes = max($vendes);

echo "<table>";
echo "<tr>";
echo "<td>";
echo "<h1 class='title'>VIDEO GAMES</h1>";
echo "<p class='subtitle'>Best Selling video Games Consoles 1983 - 2004</p>";
echo "<table>";

foreach ($vendes as $consola => $total_vendes) {
    $imatge = ($total_vendes / $max_vendes) * 500; 
    echo "<tr>";
    echo "<td>$consola</td>";
    echo "<td>";
    echo "<div style='display: flex; align-items: center;'>";
    echo "<div style='background-image: url(green.png); background-repeat: repeat-x; background-size: contain; width: {$imatge}px; height: 20px;'></div>";
    echo "<span style='margin-left: 5px;'>$total_vendes</span>";
    echo "</div>";
    echo "</td>";
    echo "</tr>";
}

echo "</table>";
echo "</td>";
echo "</tr>";
echo "</table>";
?>
