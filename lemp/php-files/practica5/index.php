<!DOCTYPE HTML>
<html>
<head></head>
<body>
    <h2>PHP Form Validation Example</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        Name: <input type="text" name="name" />
        <br />
        <br />
        E-mail: <input type="text" name="email" />
        <br />
        <br />
        Website: <input type="text" name="website" />
        <br />
        <br />
        Comment: <textarea name="comment" rows="5" cols="40"></textarea>
        <br />
        <br />
        Gender:
        <input type="radio" name="gender" value="female" />
        Female
        <input type="radio" name="gender" value="male" />
        Male
        <input type="radio" name="gender" value="other" />
        Other
        <br />
        <br />
        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike" />
        <label for="vehicle1"> I have a bike</label>
        <br />
        <input type="checkbox" id="vehicle2" name="vehicle2" value="Car" />
        <label for="vehicle2"> I have a car</label>
        <br />
        <br />
        <label for="birthday">Birthday:</label>
        <input type="date" id="birthday" name="birthday" />
        <br />
        <br />
        <label for="favcolor">Select your favorite color:</label>
        <input type="color" id="favcolor" name="favcolor" value="#ff0000" />
        <br />
        <br />
        <input type="submit" name="submit" value="Submit" />
    </form>
</body>
</html>