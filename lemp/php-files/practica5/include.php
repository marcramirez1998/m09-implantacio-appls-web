<?php
$len = 30;
$len2 = 20;
$randomArray1 = array();
$randomArray2 = array();

// Taula 1 len 30
echo "<h1>Array aleatori</h1>";
echo "<table>";
echo "<tr>";
for ($i = 0; $i < $len; $i++) {
    $randomArray1[$i] = rand(100, 999);
    echo "<td>$i</td>";
}
echo "</tr>";
echo "<tr>";
for ($i=0; $i < $len ; $i++) { 
    echo "<td>$randomArray1[$i]</td>";
}
echo "</tr>";
echo "</table>";

// Taula 2 len 20 parell i imparells
echo "<br>";
echo "<h1>Array parells i imparells</h1>";
echo "<table>";
echo "<tr>";
for ($i = 0; $i < $len2; $i++) {
    $randomArray2[$i] = rand(100, 999);
    echo "<td>$i</td>";
}
echo "</tr>";
echo "<tr>";

$pares = array();
$impares = array();
$contador = 0;

for ($i = 0; $i < $len2; $i++) {
    $randomNumber = rand(100, 999);
    if ($contador < $len2 / 2 && $randomNumber % 2 == 0) {
        $pares[] = $randomNumber;
        echo "<td>$randomNumber</td>";
        $contador++;
    } elseif ($contador >= $len2 / 2 && $randomNumber % 2 != 0) {
        $impares[] = $randomNumber;
        echo "<td>$randomNumber</td>";
        $contador++;
    } else {
        $i--;
    }
}

echo "</tr>";
echo "</table>";
echo "<br>";

// Sumas
echo "<h1>Suma Array</h1>";
$suma_pares = array_sum($pares);
echo "Suma dels numeros parells:<br>";
echo "$suma_pares <br>";
echo "<br>";

$suma_impares = array_sum($impares);
echo "Suma dels numeros imparells:<br>";
echo "$suma_impares<br>";
echo "<br>";

$suma_total = array_sum($randomArray2);
echo "Suma total de tots els numeros:<br>";
echo "$suma_total<br>";
echo "<br>";

// Average
echo "<h1>Average</h1>";
$promedio_total = $suma_total / $len2;
echo "Total Average:<br>";
echo "$promedio_total<br>";
echo "<br>";

// Array multiples de 10
echo "<h1>Array multiples de 10</h1>";
echo "<table>";
echo "<tr>";
foreach ($randomArray2 as $indice => $numero) {
    if ($numero % 10 == 0) {
        echo "<td>$indice</td>";
    }
}
echo "</tr>";
echo "<tr>";
foreach ($randomArray2 as $numero) {
    if ($numero % 10 == 0) {
        echo "<td>$numero</td>";
    }
}
echo "</tr>";
echo "</table>";
echo "<br>";

// Calcular mitja
$mitjana = array_sum($randomArray2) / count($randomArray2);

// Taula num mes grans mitjana
echo "<h1>Numeros mes grans que la mitjana</h1>";
echo "<table>";
echo "<tr>";
foreach ($randomArray2 as $index => $valor) {
    if ($valor > $mitjana) {
        echo "<td>$index</td>";
    }
}
echo "</tr>";
echo "<tr>";
foreach ($randomArray2 as $valor) {
    if ($valor > $mitjana) {
        echo "<td>$valor</td>";
    }
}
echo "</tr>";
echo "</table>";
echo "<br>";

// Taula num menors mitjana
echo "<h1>Numeros mes petits que la mitjana</h1>";
echo "<table>";
echo "<tr>";
foreach ($randomArray2 as $index => $valor) {
    if ($valor < $mitjana) {
        echo "<td>$index</td>";
    }
}
echo "</tr>";
echo "<tr>";
foreach ($randomArray2 as $valor) {
    if ($valor < $mitjana) {
        echo "<td>$valor</td>";
    }
}
echo "</tr>";
echo "</table>";

?>
